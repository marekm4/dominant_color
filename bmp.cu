#include <stdlib.h>
#include "bmp.h"

Image *load_bmp_image(FILE *file) {
    Image *image = (Image *) malloc(sizeof(Image));

    int offset;
    fseek(file, 10, SEEK_SET);
    fread(&offset, 4, 1, file);
    fseek(file, 18, SEEK_SET);
    fread(&image->width, 4, 1, file);
    fseek(file, 22, SEEK_SET);
    fread(&image->height, 4, 1, file);

    // https://en.wikipedia.org/wiki/BMP_file_format#Pixel_storage
    int padding = (4 - (image->width * 3) % 4) % 4;

    image->pixels = (unsigned char *) malloc(image->width * image->height * 3);

    // move to bitmap begining
    fseek(file, offset, SEEK_SET);

    for (int i = 0; i < image->height; i++) {
        for (int j = 0; j < image->width; j++) {
            unsigned char r, g, b;
            fread(&b, 1, 1, file);
            fread(&g, 1, 1, file);
            fread(&r, 1, 1, file);
            int pos = (image->height - i - 1) * image->width + j;
            image->pixels[(pos * 3) + 0] = r;
            image->pixels[(pos * 3) + 1] = g;
            image->pixels[(pos * 3) + 2] = b;
        }
        fseek(file, padding, SEEK_CUR);
    }

    return image;
}