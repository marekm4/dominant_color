#ifndef _BMP_H
#define _BMP_H 1

#include <stdio.h>
#include "image.h"

Image *load_bmp_image(FILE *file);

#endif