#include <stdlib.h>
#include <math.h>
#include "color.h"

typedef struct {
    double r, g, b, count;
} Bucket;

size_t get_colors(unsigned char *pixels, size_t n, unsigned char *colors, size_t m, int cuda, int iterations) {
    return get_colors_with_config(pixels, n, colors, m, .01, cuda, iterations);
}

int bucket_comparator(const void *s1, const void *s2);

void calculate_buckets(unsigned char *pixels, size_t pixel_count, Bucket *buckets) {
    for (int n = 0; n < pixel_count; n++) {
        int r = pixels[n * 3 + 0];
        int g = pixels[n * 3 + 1];
        int b = pixels[n * 3 + 2];
        int i = r >> 7;
        int j = g >> 7;
        int k = b >> 7;
        int bucket = i * 4 + j * 2 + k;
        buckets[bucket].r += r;
        buckets[bucket].g += g;
        buckets[bucket].b += b;
        buckets[bucket].count++;
    }
}

__global__ void calculate_buckets_cuda(unsigned char *pixels, size_t pixel_count, Bucket *buckets) {
    for (int n = 0; n < pixel_count; n++) {
        int r = pixels[n * 3 + 0];
        int g = pixels[n * 3 + 1];
        int b = pixels[n * 3 + 2];
        int i = r >> 7;
        int j = g >> 7;
        int k = b >> 7;
        int bucket = i * 4 + j * 2 + k;
        buckets[bucket].r += r;
        buckets[bucket].g += g;
        buckets[bucket].b += b;
        buckets[bucket].count++;
    }
}

size_t get_colors_with_config(unsigned char *pixels, size_t n, unsigned char *colors, size_t m, double small_bucket, int cuda, int iterations) {
    int pixel_count = n / 3;

    Bucket buckets[8] = {{0, 0, 0, 0},
                         {0, 0, 0, 0},
                         {0, 0, 0, 0},
                         {0, 0, 0, 0},
                         {0, 0, 0, 0},
                         {0, 0, 0, 0},
                         {0, 0, 0, 0},
                         {0, 0, 0, 0}};

    if (cuda) {
        unsigned char *device_pixels;
        Bucket *device_buckets;

        cudaError(cudaMalloc((void**)&device_pixels, sizeof(unsigned char) * n));
        cudaError(cudaMalloc((void**)&device_buckets, sizeof(Bucket) * 8));

        cudaError(cudaMemcpy(device_pixels, pixels, sizeof(unsigned char) * n, cudaMemcpyHostToDevice));
        cudaError(cudaMemcpy(device_buckets, buckets, sizeof(Bucket) * 8, cudaMemcpyHostToDevice));

        for (int i = 0; i < iterations; i++) {
            memset((void*)buckets, 0, sizeof(Bucket) * 8);
            cudaError(cudaMemcpy(device_buckets, buckets, sizeof(Bucket) * 8, cudaMemcpyHostToDevice));
            calculate_buckets_cuda<<<1, 1>>>(device_pixels, pixel_count, device_buckets);
        }

        cudaError(cudaMemcpy(buckets, device_buckets, sizeof(Bucket) * 8, cudaMemcpyDeviceToHost));

        cudaFree(device_pixels);
        cudaFree(device_buckets);
    } else {
        for (int i = 0; i < iterations; i++) {
            memset((void*)buckets, 0, sizeof(Bucket) * 8);
            calculate_buckets(pixels, pixel_count, buckets);
        }
    }

    Bucket buckets_averages[8] = {{0, 0, 0, 0},
                                  {0, 0, 0, 0},
                                  {0, 0, 0, 0},
                                  {0, 0, 0, 0},
                                  {0, 0, 0, 0},
                                  {0, 0, 0, 0},
                                  {0, 0, 0, 0},
                                  {0, 0, 0, 0}};
    int buckets_averages_count = 0;
    for (int i = 0; i < 8; i++) {
        Bucket current_bucket = buckets[i];
        if (current_bucket.count > 0) {
            buckets_averages[buckets_averages_count].r = current_bucket.r / current_bucket.count;
            buckets_averages[buckets_averages_count].g = current_bucket.g / current_bucket.count;
            buckets_averages[buckets_averages_count].b = current_bucket.b / current_bucket.count;
            buckets_averages[buckets_averages_count].count = current_bucket.count;
            buckets_averages_count++;
        }
    }

    qsort(buckets_averages, buckets_averages_count, sizeof(Bucket), bucket_comparator);

    int output_colors_count = 0;
    for (int i = 0; i < buckets_averages_count && i < m / 3; i++) {
        if (buckets_averages[i].count / (double) pixel_count > small_bucket) {
            colors[i * 3 + 0] = round(buckets_averages[i].r);
            colors[i * 3 + 1] = round(buckets_averages[i].g);
            colors[i * 3 + 2] = round(buckets_averages[i].b);
            output_colors_count++;
        }
    }

    return output_colors_count;
}

int bucket_comparator(const void *bucket1, const void *bucket2) {
    return ((Bucket *) bucket2)->count - ((Bucket *) bucket1)->count;
}
