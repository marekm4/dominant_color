#ifndef _COLOR_H
#define _COLOR_H 1

#include <stdio.h>

size_t get_colors(unsigned char *pixels, size_t n, unsigned char *colors, size_t m, int cuda, int iterations);

size_t get_colors_with_config(unsigned char *pixels, size_t n, unsigned char *colors, size_t m, double small_bucket, int cuda, int iterations);

double saturation(unsigned char r, unsigned char g, unsigned char b);

#endif
