#include <stdlib.h>
#include "image.h"

void free_image(Image *image) {
    free(image->pixels);
    free(image);
}