#ifndef _IMAGE_H
#define _IMAGE_H 1

typedef struct {
    int width, height;
    unsigned char *pixels;
} Image;

void free_image(Image *image);

#endif