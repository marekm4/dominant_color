#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"
#include "bmp.h"
#include "color.h"

int main(int argc, char *argv[]) {
    FILE *handle;
    int cuda, iterations;
    if (argc == 4) {
        handle = fopen(argv[1], "rb");
        cuda = atoi(argv[2]);
        iterations = atoi(argv[3]);
    } else {
        perror("Usage: color <file_name> <use_cuda> <iterations>\nExample: color Fotolia_45549559_320_480.bmp 1 1000\n");
        return EXIT_FAILURE;
    }

    if (!handle) {
        perror("Unable to open file");
        return EXIT_FAILURE;
    }

    Image *image = load_bmp_image(handle);

    int max = 8;
    unsigned char *colors = (unsigned char *) malloc(max * 3);

    int count = get_colors(image->pixels, image->width * image->height * 3, colors, max * 3, cuda, iterations);

    printf("<img src=\"%s\">\n<br>\n", argv[1]);
    for (int k = 0; k < count * 3; k += 3) {
        printf("<div style=\"display: inline-block; width: 40px; height: 40px; background-color: rgb(%i, %i, %i);\"></div>",
               colors[k], colors[k + 1], colors[k + 2]);
    }

    free(colors);
    free_image(image);
    fclose(handle);

    return EXIT_SUCCESS;
}